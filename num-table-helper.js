var configs = new Object();

var jqTBODY = $("tbody");
for (var iy = 0; iy < 22; iy += 1) {
  var jqTR = $("<tr>").attr("yIdx", iy).appendTo(jqTBODY);
  $("<th>").append((iy + 1).toPaddedString(2)).appendTo(jqTR);
  for (var ix = 0; ix < 5; ix += 1) {
    var seq = [ix.toPaddedString(1, 10, "x"), iy.toPaddedString(2, 10, "y")].join("_");
    var nid = (ix * 22) + iy;
    var jqIMG = $("<img>")
                .on("error", function(jqEvt){
                  jqEvt.target.setAttribute("src", "");
                })
                .attr("id", "img" + nid.toString());
    $("<td>").addClass("img").attr("yIdx", iy).attr("xIdx", ix).appendTo(jqTR).append(jqIMG);
    var jqTD = $("<td>").addClass("number").attr("xIdx", ix).attr("yIdx", iy).appendTo(jqTR);
    $("<input>").addClass("page").addClass("number")
                .attr("type", "text")
                .attr("xIdx", ix).attr("yIdx", iy)
                .attr("seq", seq).attr("id", nid.toString())
                .appendTo(jqTD);
  }
}

var updateCurTableByConfigData = function() {
  var curPage = parseInt($("#current").val()).toPaddedString(3);
  var elmINPUTs = document.querySelectorAll("input.page.number");
  for (var i = 0; i < elmINPUTs.length; i += 1) {
    var jqINPUT = $(elmINPUTs[i]);
    var jqIMG = $(elmINPUTs[i].parentElement.previousSibling.children[0]);
    jqIMG.attr("src", ["cropPageNum", curPage, jqINPUT.attr("seq") + ".png"].join("/") );
    var k = curPage + jqINPUT.attr("seq").replace("x", "#").replace("_y", "#");
    if (configs.gocr[k]) {
      jqINPUT.val( configs.gocr[k].toString() );
    } else {
      jqINPUT.val("");
    };
  };
  $("a#href2json").hide();
};

var updateConfigDataByCurTable = function() {
  var curPage = parseInt($("#current").val());
  var elmINPUTs = document.querySelectorAll("input.page.number");
  for (var i = 0; i < elmINPUTs.length; i += 1) {
    var jqINPUT = $(elmINPUTs[i]);
    var k = curPage.toPaddedString(3) + jqINPUT.attr("seq").replace("x", "#").replace("_y", "#");
    if (jqINPUT.val()) {
      configs.gocr[k] = jqINPUT.val();
    };
  };
  $("a#href2json").hide();
};

$("#current").change(function(){
  updateCurTableByConfigData();
});

$("#prev").click(function(){
  var curPage = parseInt($("#current").val());
  updateConfigDataByCurTable();
  $("#current").val(curPage - 1);
  updateCurTableByConfigData();
});

$("#next").click(function(){
  var curPage = parseInt($("#current").val());
  updateConfigDataByCurTable();
  $("#current").val(curPage + 1);
  updateCurTableByConfigData();
});

$(window).keydown(function(jqEvt){
  if (jqEvt.keyCode == 9) { // TAB
    var elmActive = document.activeElement;
    if (elmActive.tagName != "INPUT") {
      return true;
    };
    var id = elmActive.getAttribute("id");
    if (!/^[0-9]+$/.test(id)) {
      return true;
    };
    var nid = parseInt(id);

    var dy = 1;
    if (jqEvt.shiftKey)
      dy = -1;

    id = ((nid + dy) % (22 * 5)).toString();
    $(document.getElementById(id)).focus();
    jqEvt.preventDefault();
    return false;
  }
});

$("#save").click(function(){
  var js = JSON.stringify(configs.gocr);
  var b = window.URL.createObjectURL(window.genBlob([js], "text/json"));
  $("a#href2json").prop("target", "_blank")
                  .prop("href", b)
                  .click(function(){return false;})
                  .text("JSON in " + js.length.toString(10) + " bytes")
                  .show();
});

loadConfigs(["gocr.json"])
.then(function(promise){
   updateCurTableByConfigData();
});
