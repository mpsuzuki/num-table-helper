// -----------------------------------
// polyfill
//
//  String.repeat(unsigned int)
if (!String.repeat) {
  String.prototype.repeat = function(count) {
    var toks = [];
    while (count > 0) {
      toks.push( this );
      count -= 1;
    };
    return toks.join("");
  };
};

String.prototype.dirname = function() {
  var toks = this.split("/");
  toks.pop();
  return toks.join("/");
};

String.prototype.basename = function(suffix) {
  var toks = this.split("/");
  var b = toks.pop();
  if (suffix && (b.slice(-(suffix.length)) == suffix)) {
    return b.substring(0, b.length - suffix.length);
  }
  return b;
};

// -------------------------------------------------------------------

// browser dependency of Blob builder
if (Blob) {
  window.genBlob = function(blobDataArr, blobMimeType) {
    return (new Blob(blobDataArr, {type: blobMimeType}));
  };
} else if (window.MozBlobBuilder != undefined ) {
  window.genBlob = function(blobDataArr, blobMimeType) {
    var blobBuilder = new window.MozBlobBuilder();
    for (var i = 0; i < blobDataArr.length; i += 1) {
      blobBuilder.append(blobDataArr[i]);
    }
    return blobBuilder.getBlob(blobMimeType);
  };
} else if (window.WebKitBlobBuilder != undefined) {
  window.genBlob = function(blobDataArr, blobMimeType) {
    var blobBuilder = new window.WebKitBlobBuilder();
    for (var i = 0; i < blobDataArr.length; i += 1) {
      blobBuilder.append(blobDataArr[i]);
    }
    return blobBuilder.getBlob(blobMimeType);
  };
}

// browser dependency of window.URL
if (!window.URL && window.webkitURL) {
  window.URL = window.webkitURL;
}

// -----------------------------------
// something like sprintf("%03d"), sprintf("%04x")
//
Number.prototype.toPaddedString = function(width, base, prefix) {
  if (!base) {
    base = 10;
  };
  if (!prefix) {
    if (base == 16)
      prefix = "0x";
    else
      prefix = "";
  };

  var s = this.toString(base).toUpperCase();
  if (s.length < width)
    return prefix + ("0".repeat(width) + s).slice(-width);
  else
    return prefix + s;
};

// -----------------------------------
// XHR utilities
//
getPromiseToFetchFromUrls = function(urls, rets){
  var promise = new Promise(function(resolveFunc, rejectFunc){
    urls.forEach(function(aUrl){
      var xhr = new XMLHttpRequest();
      xhr.open("GET", aUrl, true);
      xhr.onload = function() {
        if (this.readyState == 4 && (this.status == 200 || this.status == 0)) {
          rets.push( [this.responseURL, this.responseText] );

          if (urls.length == rets.length)
            resolveFunc();
        } else
          rejectFunc();
      };
      xhr.send();
    });
  });
  return promise;
};

var loadConfigs = function(urls) {
  var configJsons = [];
  var promise = getPromiseToFetchFromUrls(urls, configJsons);
  promise.then(function(){
    configJsons.forEach(function(a){
      var url = a[0];
      var b = url.basename().replace(/\.[Jj][Ss][Oo][Nn]/, "");
      var jsText = a[1];
      var jsObj = JSON.parse(jsText);

      if (!configs[b])
        configs[b] = new Object();

      for (var k in jsObj)
        configs[b][k] = jsObj[k];
    });
  });
  return promise;
};
